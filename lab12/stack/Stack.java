package stack;

import java.util.Objects;

public interface Stack {
    void push(Object item);
    Object pop();
    boolean empty();
}
