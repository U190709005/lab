public class FindPrimes {
    public static void main(String[] args){
        System.out.println("Max= " + args[0]);
        int max = Integer.parseInt(args[0]);
        for(int number = 2 ; number<max;number++ ){
            int divisors = 2;
            boolean isPrime = true;

            while (divisors<number && isPrime){
                if(number % divisors == 0 ){
                    isPrime = false;
                }
                divisors++;
            }
            if(isPrime){
                System.out.print(number + ",");
            }
        }
    }
}
