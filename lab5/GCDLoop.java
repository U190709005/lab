public class GCDLoop {
    public static void main(String[] args){
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);
        int result = gcd(number1,number2);
        System.out.println("GCD of " + number1 + " and " + number2 + " = "+ result);
    }
    public static int gcd(int n1, int n2){
        int remainder;
        do{
            remainder = n1 % n2;
            n1 = n2;
            n2 = remainder;
        }while (remainder != 0);

        return n1;


    }
}
