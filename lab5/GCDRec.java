public class GCDRec {
    public static void main(String[] args){
        int number1 = Integer.parseInt(args[0]);
        int number2 = Integer.parseInt(args[1]);
        int result = gcd(number1,number2);
        System.out.println("GCD of " + number1 + " and " + number2 + " = "+ result);
    }
    public static int gcd(int n1, int n2){
        int remainder = n1 % n2;
        if(n1 % n2 == 0){
            return n2;
        }
        return gcd(n2,remainder);

    }
}
