public class MyDateTime {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        time.incrementHour();
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);

    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if(dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementMonth(int diff) {
        date.incrementMonth(diff);
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if(date.isBefore(anotherDateTime.date)){
            return true;
        }else if(date.isAfter(anotherDateTime.date)){
            return false;
        }
        if(time.isBefore(anotherDateTime.time))
            return true;
        return false;
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if(date.isAfter(anotherDateTime.date))
            return true;
        else if(date.isBefore(anotherDateTime.date))
            return false;
        if(time.isAfter(anotherDateTime.time))
            return true;
        return false;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        if(date.dayDifference(anotherDateTime.date) == 0){
            return (time.hour*60 + time.minute)>(anotherDateTime.time.hour*60 + anotherDateTime.time.minute)?
                    String.valueOf(((time.hour*60 + time.minute)-(anotherDateTime.time.hour*60 + anotherDateTime.time.minute))/60):
                    ((anotherDateTime.time.hour*60 + anotherDateTime.time.minute)-(time.hour*60 + time.minute))/60 +
                    " hour(s)";
        }else{
            if(date.isBefore(anotherDateTime.date)){
                int dateMin;
                int realHourDiff = 0;
                int realMinDiff =0;
                if(time.hour ==0)
                    dateMin = 23*60+time.minute;
                else
                    dateMin = time.hour*60 + time.minute;
                int anotherDateMin;
                if(anotherDateTime.time.hour == 0)
                    anotherDateMin = 23*60+anotherDateTime.time.minute;
                else
                    anotherDateMin = anotherDateTime.time.hour*60 + anotherDateTime.time.minute;

                if(dateMin > anotherDateMin){
                    int hourDiff;
                    int minuteDiff;
                    if(time.minute<anotherDateTime.time.minute){
                        time.minute = time.minute + 60;
                        time.hour = time.hour -1;
                        hourDiff = (time.hour - anotherDateTime.time.hour);
                        minuteDiff = (time.minute-anotherDateTime.time.minute);
                    }else{
                        hourDiff = time.hour - anotherDateTime.time.hour;
                        minuteDiff = time.minute - anotherDateTime.time.minute;
                    realHourDiff = 23 - hourDiff;
                    realMinDiff = 60 - minuteDiff;
                    }return String.valueOf(date.dayDifference(anotherDateTime.date)-1) + " day(s) "+
                            String.valueOf(realHourDiff) + " hour(s) "
                            + String.valueOf(realMinDiff) + " minute(s)";
                }
            }else if(date.isAfter(anotherDateTime.date)){
                return "First date must lesser than second date.";
            }
        }
        return null;
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }

    public void decrementMonth(int diff) {
        date.decrementMonth(diff);
    }

    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }

    public void decrementYear(int diff) {
        date.decrementYear(diff);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if(dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public String toString(){
        return date.toString() + " " + time.toString();
    }
}
